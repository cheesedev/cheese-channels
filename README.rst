=======================================
Cheese Channels - The heart of your CMS
=======================================


Quick start
-----------

1. Instale o aplicativo::

    pip install https://bitbucket.org/cheesedev/cheese-channels/raw/a64119cf3bb4a689e9c9930d1702c43931d24af5/dist/channels-0.1.tar.gz

2. Adicione "channels" as configurações INSTALLED_APPS como abaixo::

    INSTALLED_APPS = (
        ...
        'channels',
    )

3. Informe onde os modelos de templates em `CHANNEL_TEMPLATES`, como abaixo::

    CHANNEL_TEMPLATES = (
        # Busca em TEMPLATES + 'base1/'
        ('base1', 'Base 1'),
        # Busca em TEMPLATES + 'bases/base2/'
        ('bases/base2', 'Base 2'),
    )

4. Adicione a sua configuração de `MIDDLEWARE_CLASSES` o middleware de templates::

    MIDDLEWARE_CLASSES = (
        # ...

        'channels.middleware.ChannelTemplateMiddleware',
    )

5. Execute `python manage.py migrate` para criar os modelos de `channels`.


Como funciona
-------------

O canal é uma extensão de página para que possamos armazenar informações de configuração de um determinado canal, dessa forma podemos trabalhar diversas funcionalidades externas, como por exemplo a troca de templates padrão.

Criando e configurando um canal
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

1. Acesse no menu de preview a opção ``Página``
2. Escolha o menu ``Definições Avançadas``
3. Marque sua página como ``Soft root``

4. A partir de um página filha ou da própria página que represente o Canal, acesso o menu ``Página``.
5. Escolha o menu ``Edit channel settings``
6. Selecione o ``Template base`` em que seu Canal irá renderizar.


Apphook, entendendo o Template Base
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Para que seu apphook consiga renderizar de acordo com o ``Template Base`` definido pelo Canal, é necessário extender suas views de `channels.views.ChannelMixin`, como no código abaixo::

    from django.views.generic.base import TemplateView

    from channels.views import ChannelMixin


    class HomeView(ChannelMixin, TemplateView):
        template_name = 'contact/home.html'
