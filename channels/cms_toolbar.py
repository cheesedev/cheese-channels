# coding: utf-8
from django.core.urlresolvers import reverse
from django.utils.translation import ugettext_lazy as _

from cms.toolbar_pool import toolbar_pool
from cms.extensions.toolbar import ExtensionToolbar

from .models import ChannelExtension
# from .utils import search_channel


def get_extension(page):
    extension = ChannelExtension.objects.filter(extended_object=page).first()
    if not page or extension:
        return extension

    return get_extension(page.parent)


@toolbar_pool.register
class ChannelExtensionToolbar(ExtensionToolbar):
    model = ChannelExtension

    def populate(self):
        # import ipdb; ipdb.set_trace()
        current_page_menu = self._setup_extension_toolbar()
        current_page = self.page

        if current_page_menu and current_page:
            channels_menu = current_page_menu.get_or_create_menu(
                'channels', _(u'Definições de Canal'), position=1)

            page_extension, url = self.get_page_extension_admin()
            if not page_extension:
                page_extension = get_extension(current_page)

            channels_menu.add_modal_item(_(u'Novo Canal'), url=url)

            if page_extension:
                opts = ChannelExtension._meta
                change_url = reverse(
                    'admin:%s_%s_change' % (opts.app_label, opts.model_name),
                    args=[page_extension.pk,])
                channels_menu.add_modal_item(_(u'Editar Canal'),
                                             url=change_url)
