def search_channel(page):
    """
    Recursive search the page soft_root in ancestors.
    """
    if not page or (hasattr(page, 'channelextension') and page.channelextension):
        return page

    return search_channel(page.parent)
