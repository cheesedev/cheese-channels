import os


class ChannelMixin(object):

    def get_template_names(self):
        template_names = super(ChannelMixin, self).get_template_names()

        if type(template_names) == str or type(template_names) == unicode:
            return template_names

        if getattr(self.request, 'template_base', False):
            base = getattr(self.request, 'template_base')
            template_base = os.path.join(base, self.template_name)
            template_names.insert(0, template_base)

        return template_names
