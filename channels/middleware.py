from .utils import search_channel


class ChannelTemplateMiddleware(object):

    def process_request(self, request):
        page = search_channel(request.current_page)
        extension = getattr(page, 'channelextension', None)

        if extension is not None:
            setattr(request, 'template_base', extension.template_base)
