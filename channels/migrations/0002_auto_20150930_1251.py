# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('channels', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='channelextension',
            name='template_base',
            field=models.CharField(blank=True, max_length=200, null=True),
        ),
    ]
