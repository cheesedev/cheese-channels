from django.contrib import admin
# from django.contrib.admin.options import csrf_protect_m
# from django.core.urlresolvers import reverse
# from django.http import HttpResponseRedirect

# from cms.models import Page
# from cms.admin.pageadmin import PageAdmin
from cms.extensions import PageExtensionAdmin

from .forms import ChannelExtensionForm
from .models import ChannelExtension
# from .utils import search_channel


class ChannelExtensionAdmin(PageExtensionAdmin):
    form = ChannelExtensionForm

    # @csrf_protect_m
    # def add_view(self, request, form_url='', extra_context=None):
    #     extended_object_id = request.GET.get('extended_object', False)
    #     if extended_object_id:
    #         try:
    #             page = Page.objects.get(pk=extended_object_id)
    #             page = search_channel(page)

    #             extension = self.model.objects.get(extended_object=page)
    #             opts = self.model._meta
    #             change_url = reverse(
    #                 'admin:%s_%s_change' % (opts.app_label, opts.model_name),
    #                 args=(extension.pk,),
    #                 current_app=self.admin_site.name
    #             )
    #             return HttpResponseRedirect(change_url)
    #         except self.model.DoesNotExist:
    #             pass

    #     return super(ChannelExtensionAdmin,
    #                  self).add_view(request, form_url, extra_context)

admin.site.register(ChannelExtension, ChannelExtensionAdmin)


# class CustomPageAdmin(PageAdmin):
#     list_display = ['changed_by']

# admin.site.unregister(Page)

# admin.site.register(Page, CustomPageAdmin)
