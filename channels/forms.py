from django import forms
from django.conf import settings

from .models import ChannelExtension
from .utils import search_channel

from cms.admin import forms as cms_forms
from cms.utils import get_cms_setting


class ChannelExtensionForm(forms.ModelForm):
    template_base = forms.ChoiceField(
        choices=getattr(settings, 'CHANNEL_TEMPLATES', ()),
        required=False
    )

    class Meta:
        model = ChannelExtension
        fields = ['template_base', ]


init_advanced = cms_forms.AdvancedSettingsForm.__init__


def contains_inlist(value, list):
    for item in list:
        if item in value:
            return True
    return False


def channel_init_form(self, *args, **kwargs):
    init_advanced(self, *args, **kwargs)
    ch_templates = getattr(settings, 'CH_TEMPLATES', False)
    cms_templates = get_cms_setting("TEMPLATES")
    page = kwargs.get("instance", None)
    page_channel = search_channel(page) if page else None

    if page_channel and ch_templates:
        channel = page_channel.channelextension
        if channel.template_base in ch_templates:
            templates = ch_templates.get(channel.template_base)
            self.fields['template'].choices = [
                (i, k) for i,k in cms_templates if contains_inlist(i, templates)
            ]

cms_forms.AdvancedSettingsForm.__init__ = channel_init_form
