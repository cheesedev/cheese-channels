import os

from django.template import TemplateDoesNotExist
from django.template.loader import get_template

from cms.plugin_base import CMSPluginBase

from .cms_toolbar import get_extension


class ChannelPluginBase(CMSPluginBase):
    error_template = 'channels/template_does_not_exist.html'

    def get_render_template(self, context, instance, placeholder):
        request = context.get('request')

        extension = get_extension(request.current_page)
        if extension is not None:
            render_template = os.path.join(extension.template_base,
                                           self.render_template)

            return self.get_name_template(render_template,
                                          self.render_template)

        return self.get_name_template(self.render_template)

    def get_name_template(self, *templates):
        for template in templates:
            try:
                get_template(template)
                return template
            except TemplateDoesNotExist:
                pass

        return self.error_template
