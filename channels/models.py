from django.db import models

from cms.extensions import PageExtension
from cms.extensions.extension_pool import extension_pool

from django.conf import settings
from cms.models import Page


class ChannelExtension(PageExtension):
    template_base = models.CharField(
        max_length=200,
        blank=True,
        null=True
    )

extension_pool.register(ChannelExtension)
